from soc_drf_backend.settings.base import *

BOT_SETTINGS={
    'numbers_of_users':3,
    'max_posts_per_user': 8,
    'max_likes_per_user': 4
}