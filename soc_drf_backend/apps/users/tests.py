from django.urls import reverse
from rest_framework import status
from rest_framework.test import APITestCase
from django.contrib.auth.models import User


class AuthViewsTests(APITestCase):

    def setUp(self):
        self.username = 'testcaseuser'
        self.password = 'testcasepassword'
        self.data = {
            'username': self.username,
            'password': self.password
        }

    def test_user_sign_up(self):
        data = {
            'username': 'TestUserCreate',
            'password': 'password',
            'email': 'test@mail.com'
        }
        response = self.client.post(reverse('sign-up-list'), data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED, response.content)

    def test_user_token(self):
        user = User.objects.create_user(username=self.username, email='testcaseuser@mail.com', password=self.password)
        self.assertEqual(user.is_active, 1, 'Active User')
        response = self.client.post(reverse('sign-in'), self.data, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK, response.content)
        token = response.data.get('access')
        self.client.credentials(HTTP_AUTHORIZATION=f'Bearer {token}')
        response = self.client.get(reverse('user-list'), data={'format': 'json'})
        self.assertEqual(response.status_code, status.HTTP_200_OK, response.content)

    def test_user_update(self):
        user = User.objects.create_user(username=self.username, email='testcaseuser@mail.com', password=self.password)
        response = self.client.post(reverse('sign-in'), self.data, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK, response.content)
        token = response.data.get('access')
        self.client.credentials(HTTP_AUTHORIZATION=f'Bearer {token}')
        self.assertEqual(user.is_active, 1, 'Active User')
        data = {"email": "update_email@mail.com"}
        response = self.client.patch(reverse('user-detail', kwargs={"pk": user.id}), data, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK, response.content)

    def test_favorite_posts(self):
        user = User.objects.create_user(username=self.username, email='testcaseuser@mail.com', password=self.password)
        self.assertEqual(user.is_active, 1, 'Active User')
        response = self.client.post(reverse('sign-in'), self.data, format='json')
        token = response.data.get('access')
        self.client.credentials(HTTP_AUTHORIZATION=f'Bearer {token}')
        response = self.client.get(reverse('user-favorite-posts'), data={}, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK, response.content)

    def test_my_data(self):
        user = User.objects.create_user(username=self.username, email='testcaseuser@mail.com', password=self.password)
        self.assertEqual(user.is_active, 1, 'Active User')
        response = self.client.post(reverse('sign-in'), self.data, format='json')
        token = response.data.get('access')
        self.client.credentials(HTTP_AUTHORIZATION=f'Bearer {token}')
        response = self.client.get(reverse('user-my-data'), data={}, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK, response.content)

    def test_change_password(self):
        user = User.objects.create_user(username=self.username, email='testcaseuser@mail.com',
                                        password=self.password)
        response = self.client.post(reverse('sign-in'), self.data, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK, response.content)
        token = response.data.get('access')
        self.client.credentials(HTTP_AUTHORIZATION=f'Bearer {token}')
        data = {'old_password':'testcasepassword', 'new_password':'testpassnew'}
        response = self.client.put(reverse('change-password'), data=data, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK, response.content)
        user_data = self.data
        user_data.update({'password':'testpassnew'})
        response = self.client.post(reverse('sign-in'), user_data, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK, response.content)