from django.apps import AppConfig


class UserConfig(AppConfig):
    name = 'soc_drf_backend.apps.users'
