from rest_framework import serializers, exceptions
from django.conf import settings
from soc_drf_backend.apps.users.models import User
from rest_framework_simplejwt.serializers import TokenObtainPairSerializer


class UserManagerSerializer(serializers.ModelSerializer):
    user = serializers.HiddenField(default=serializers.CurrentUserDefault())

    class Meta:
        model = User
        exclude = ['password']


class SignUpSerializers(serializers.ModelSerializer):

    def create(self, validated_data):
        password = validated_data.get('password')
        instance = super().create(validated_data)
        instance.set_password(password)
        instance.save()
        return instance

    def update(self, instance, validated_data):
        password = validated_data.pop('password', None)
        if password:
            instance.set_password(password)
            instance.save()
        return super().update(instance, validated_data)

    class Meta:
        model = User
        extra_kwargs = {'password': {'write_only': True}}
        fields = ['username', 'email', 'password']


class CustomTokenObtainPairSerializer(TokenObtainPairSerializer):

    def validate(self, attrs):
        data = super().validate(attrs)
        data['types'] = list(settings.SIMPLE_JWT.get('AUTH_HEADER_TYPES'))
        data['user'] = UserManagerSerializer(self.user).data
        return data


class ChangePasswordSerializer(serializers.Serializer):
    user = serializers.HiddenField(default=serializers.CurrentUserDefault())
    old_password = serializers.CharField(required=True)
    new_password = serializers.CharField(required=True)

    def validate(self, attrs):
        data = super().validate(attrs)
        old_password = data.get('old_password', None)
        user = data.get('user', None)
        if not user.check_password(old_password):
            raise exceptions.ValidationError('Old password is incorrect')
        return data
