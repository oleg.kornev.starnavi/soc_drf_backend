from rest_framework_simplejwt.views import TokenRefreshView

from rest_framework.routers import DefaultRouter

from django.urls import path

from soc_drf_backend.apps.users.views import UserManagerViewSet, UserSignUpViewSet, \
    CustomTokenObtainPairView, ChangePasswordAPIView


user_router = DefaultRouter()
user_router.register('user-manager', UserManagerViewSet)
user_router.register('sign-up', UserSignUpViewSet, base_name='sign-up')

urlpatterns = [
    path('sign-in/', CustomTokenObtainPairView.as_view(), name='sign-in'),
    path('token/refresh/', TokenRefreshView.as_view(), name='token-refresh'),
    path('change-password/', ChangePasswordAPIView.as_view(), name='change-password'),
              ] +user_router.urls
