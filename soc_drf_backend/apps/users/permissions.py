from rest_framework import exceptions, permissions


class IsOwnerOrReadOnly(permissions.IsAuthenticatedOrReadOnly):

    message = 'Only the account owner or admin can edit the posts'

    def has_object_permission(self, request, view, obj):
        if request.method in permissions.SAFE_METHODS:
            return True
        return obj.user == request.user or request.user.is_staff
