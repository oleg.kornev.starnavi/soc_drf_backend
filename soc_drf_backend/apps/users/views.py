from rest_framework.response import Response
from rest_framework.viewsets import GenericViewSet
from rest_framework.generics import UpdateAPIView
from rest_framework.permissions import AllowAny
from rest_framework import mixins, decorators, status, permissions

from rest_framework_simplejwt.views import TokenObtainPairView

from soc_drf_backend.apps.users.models import User
from soc_drf_backend.apps.users.serializers import UserManagerSerializer,\
    CustomTokenObtainPairSerializer, SignUpSerializers, ChangePasswordSerializer
from soc_drf_backend.apps.users.permissions import IsOwnerOrReadOnly
from soc_drf_backend.apps.posts.serializers import PostsSerializer


class UserManagerViewSet(mixins.ListModelMixin,
                         mixins.RetrieveModelMixin,
                         mixins.UpdateModelMixin,
                         GenericViewSet):

    permission_classes = [permissions.IsAuthenticatedOrReadOnly]
    queryset = User.objects.all()
    serializer_class = UserManagerSerializer

    @decorators.action(methods=['GET'], detail=False, url_path='my-data',
                       permission_classes=[permissions.IsAuthenticated])
    def my_data(self, request, pk=None, *args, **kwargs):
        response = self.get_serializer(request.user).data
        return Response(response, status=status.HTTP_200_OK)

    @decorators.action(methods=['GET'], detail=False, url_path='favorite-posts',
                       permission_classes=[permissions.IsAuthenticated])
    def favorite_posts(self, request, pk=None, *args, **kwargs):
        #posts what user like
        user = request.user
        posts = user.likes.all()
        response = PostsSerializer(posts, many=True).data
        return Response(response, status=status.HTTP_200_OK)


class UserSignUpViewSet(mixins.CreateModelMixin,
                        GenericViewSet):

    permission_classes = [AllowAny]
    queryset = User.objects.all()
    serializer_class = SignUpSerializers


class CustomTokenObtainPairView(TokenObtainPairView):
    serializer_class = CustomTokenObtainPairSerializer


class ChangePasswordAPIView(UpdateAPIView):
    permission_classes = [permissions.IsAuthenticated]
    serializer_class = ChangePasswordSerializer
    
    def update(self, request, *args, **kwargs):
        user = request.user
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        user.set_password(request.data.get('new_password'))
        user.save()
        return Response({"message": "You changed password"}, status=status.HTTP_200_OK)



