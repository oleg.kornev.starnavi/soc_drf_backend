from django.db import models
from soc_drf_backend.apps.users.models import User


class Post(models.Model):
    user = models.ForeignKey(User, verbose_name="Author", on_delete=models.CASCADE, related_name='user')
    title = models.CharField("Title", max_length=300)
    date_create = models.DateTimeField("Create", auto_now=False, auto_now_add=True)
    date_update = models.DateTimeField("Edit", auto_now=True, auto_now_add=False)
    message = models.TextField("Message", max_length=3000)
    likes = models.ManyToManyField(User, verbose_name="Like",
                                   related_name='likes', blank=True)

    def __str__(self):
        return f'Message from <{self.user.username}> Title:{self.title}'

    class Meta:
        ordering = ['-date_update', '-date_create']
        verbose_name = 'Posts'
        verbose_name_plural = 'Posts'


