from rest_framework import serializers
from soc_drf_backend.apps.posts.models import Post


class PostsSerializer(serializers.ModelSerializer):
    user = serializers.HiddenField(default=serializers.CurrentUserDefault())


    class Meta:
        model = Post
        fields = '__all__'
        read_only_fields = ('likes',)