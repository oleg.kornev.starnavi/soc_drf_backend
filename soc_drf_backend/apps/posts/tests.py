from django.urls import reverse
from rest_framework import status
from rest_framework.test import APITestCase
from django.contrib.auth.models import User


class PostsViewsTests(APITestCase):

    def setUp(self):
        self.username = 'testcaseuser'
        self.password = 'testcasepassword'
        self.data = {
            'username': self.username,
            'password': self.password
        }

    def test_user_post_and_update(self):
        user = User.objects.create_user(username=self.username, email='testcaseuser@mail.com', password= self.password)
        self.assertEqual(user.is_active, 1, 'Active User')

        response = self.client.post(reverse('sign-in'), self.data, format='json')
        token = response.data.get('access')

        self.client.credentials(HTTP_AUTHORIZATION=f'Bearer {token}')
        data = {
            "title": "TestTestTestTest",
            "message": "TestTestTestTestTestTestTestTestTestTestTestTestTestTestTestTest",

        }
        response = self.client.post(reverse('post-list'), data=data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED, response.content)
        post_id = response.data.get('id')
        data = {
            "title": "update_test",
            "message": "update_testupdate_testupdate_testupdate_testupdate_testupdate_test",

        }
        response = self.client.patch(reverse('post-detail', kwargs={"pk": post_id}), data=data, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK, response.content)


    def test_push_like(self):
        user = User.objects.create_user(username=self.username, email='testcaseuser@mail.com', password= self.password)
        self.assertEqual(user.is_active, 1, 'Active User')
        response = self.client.post(reverse('sign-in'), self.data, format='json')
        token = response.data.get('access')
        self.client.credentials(HTTP_AUTHORIZATION=f'Bearer {token}')
        data = {
            "title": "TestTestTestTest",
            "message": "TestTestTestTestTestTestTestTestTestTestTestTestTestTestTestTest"
        }

        response = self.client.post(reverse('post-list'), data=data, format='json')
        post_id = response.data.get('id')
        response = self.client.post(reverse('post-push-like',  kwargs={'pk': post_id}), data=data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED, response.content)

    def test_get_users_who_like(self):
        user = User.objects.create_user(username=self.username, email='testcaseuser@mail.com', password= self.password)
        self.assertEqual(user.is_active, 1, 'Active User')
        response = self.client.post(reverse('sign-in'), self.data, format='json')
        token = response.data.get('access')
        self.client.credentials(HTTP_AUTHORIZATION=f'Bearer {token}')
        data = {
            "title": "TestTestTestTest",
            "message": "TestTestTestTestTestTestTestTestTestTestTestTestTestTestTestTest"
        }

        response = self.client.post(reverse('post-list'), data=data, format='json')
        post_id = response.data.get('id')
        response = self.client.post(reverse('sign-in'), self.data, format='json')
        token = response.data.get('access')
        response = self.client.get(reverse('post-get-users-who-like', kwargs={'pk': post_id}), data={}, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK, response.content)

