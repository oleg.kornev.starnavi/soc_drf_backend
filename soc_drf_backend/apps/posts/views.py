from rest_framework import decorators, status, permissions

from rest_framework.viewsets import ModelViewSet
from rest_framework.response import Response

from soc_drf_backend.apps.users.serializers import UserManagerSerializer

from soc_drf_backend.apps.posts.models import Post
from soc_drf_backend.apps.posts.serializers import PostsSerializer
from soc_drf_backend.apps.users.permissions import IsOwnerOrReadOnly


class PostManagerViewSet(ModelViewSet):

    queryset = Post.objects.all()
    serializer_class = PostsSerializer
    permission_classes = [IsOwnerOrReadOnly]

    @decorators.action(detail=True, methods=['POST'], url_path='like',
                       permission_classes=[permissions.IsAuthenticated])
    def push_like(self, request, pk=None, *args, **kwargs):
        post = self.get_object()
        user = request.user
        like = post.likes.filter(id=user.id).first()
        if not like:
            post.likes.add(user)
            return Response({"message": f"You liked post {post.title}"}, status=status.HTTP_201_CREATED)
        post.likes.remove(user)
        return Response({"message": f"You unliked post {post.title}"}, status=status.HTTP_201_CREATED)

    @decorators.action(detail=True, methods=['get'], url_path='get-who-like')
    def get_users_who_like(self, request, pk=None, *args, **kwargs):
        post = self.get_object()
        users = post.likes.all()
        response = UserManagerSerializer(users, many=True).data
        return Response(response, status=status.HTTP_200_OK)


