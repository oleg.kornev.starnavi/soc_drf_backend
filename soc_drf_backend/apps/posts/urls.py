
from rest_framework.routers import DefaultRouter
from soc_drf_backend.apps.posts.views import PostManagerViewSet


post_router = DefaultRouter()
post_router.register('post-manager', PostManagerViewSet)



urlpatterns = [

] + post_router.urls