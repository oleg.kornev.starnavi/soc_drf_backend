from django.apps import AppConfig


class PostsConfig(AppConfig):
    name = 'soc_drf_backend.apps.posts'
