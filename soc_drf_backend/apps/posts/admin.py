from django.contrib import admin
from soc_drf_backend.apps.posts.models import Post
# Register your models here.
admin.site.register(Post)