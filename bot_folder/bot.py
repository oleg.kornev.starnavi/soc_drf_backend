import requests, random, string, json

from user_agent import generate_user_agent

from bot_folder.config_file import *

class Bot():

    def __init__(self,
                 domen,
                 HTTPS=False,
                 password = 'password',
                 token_type = 'Bearer',
                 *args,
                 **kwargs):

        self.headers = {
            'Content-Type': "application/json",
            'cache-control': "no-cache"
        }
        self.__password = password
        self.__token_type = token_type + ' '
        self.__sign_up_url = None
        self.__sign_in_url = None
        self.__posting_url = None
        self.__get_posts_url = None
        self.__likes_url = None
        self.__user_tokens = []
        self.__HTTPS = HTTPS
        self.__domen = domen
        self.__numbers_of_users = NUMBERS_OF_USERS
        self.__max_posts_per_user = MAX_POSTS_PER_USERS
        self.__max_likes_per_user = MAX_LIKES_PER_USERS
        self.__timeout = TIMEOUT if TIMEOUT > 0 else None
        self.__max_retries = MAX_RETRIES if MAX_RETRIES>0 else 1
        self.__posts = []

    @property
    def password(self):
        return self.__password

    @password.setter
    def password(self, value):
        self.__password = value

    @property
    def sign_up_url(self):
        return self.__sign_up_url

    @sign_up_url.setter
    def sign_up_url(self, value):
        self.__sign_up_url = value

    @property
    def sign_in_url(self):
        return self.__sign_in_url

    @sign_in_url.setter
    def sign_in_url(self, value):
        self.__sign_in_url = value

    @property
    def posting_url(self):
        return self.__posting_url

    @posting_url.setter
    def posting_url(self, value):
        self.__posting_url = value

    @property
    def likes_url(self):
        return self.__likes_url

    @likes_url.setter
    def likes_url(self, value):
        self.__likes_url = value

    @property
    def get_posts_url(self):
        return self.__get_posts_url

    @get_posts_url.setter
    def get_posts_url(self, value):
        self.__get_posts_url = value

    @staticmethod
    def random_string(str_len=10):
        letters = string.ascii_lowercase
        return ''.join(random.choice(letters) for i in range(str_len))

    def update_headers(self, headers_data):
        self.headers.update(headers_data)

    def delete_from_headers(self, key):
        self.headers.pop(key, None)

    def make_url(self, endpoint='', detail=False, pk=None, option='', *args, **kwargs):
        type = 'http://'
        if self.__HTTPS:
            type = 'https://'
        if detail:
            return type + self.__domen + '/' + endpoint + f'{pk}/' + option + '/'
        return type + self.__domen + '/' + endpoint

    def make_requests(self, method, url, data=None, headers=None):
        for step in range(self.__max_retries):
            try:
                response = requests.request(method, url, data=data, headers=headers, timeout=self.__timeout)
                return response
            except requests.ConnectionError:
                print("Received ConnectionError. Retrying...")
                continue

    def make_payload(self, data):
        return json.dumps(data)

    def make_random_posts(self):
        posts = set()
        while len(posts) < self.__max_posts_per_user:
            posts.add(self.random_string(40))
        payloads = []
        for post in posts:
            payloads.append(
                {
                    "title": post[:10],
                    "message": post
                }
            )
        self.__posts = list(map(self.make_payload, payloads))

    def generate_user_tokens(self):
        print("Generate users:")
        users = set()
        while len(users) < self.__numbers_of_users:
            users.add(self.random_string(8))
        url = self.make_url(self.__sign_up_url)
        for user in users:
            data = {"username":user,
                    "email":user+"@gmail.com",
                    "password": self.__password}
            payload = self.make_payload(data)
            response = self.make_requests(method="POST",url=url, data=payload, headers=self.headers)
            if response.status_code == 201:
                url = self.make_url(self.__sign_in_url)
                data = {"username": user,
                        "password": self.__password}
                payload = self.make_payload(data)
                response = self.make_requests(method="POST", url=url, data=payload, headers=self.headers)
                print(response.status_code)
                if response.status_code == 200:
                    self.__user_tokens.append(self.__token_type + response.json().get('access'))

    def make_posts_by_user(self, posts=None):
        print("Generate posts:")
        url = self.make_url(self.__posting_url)
        for token in self.__user_tokens:
            self.update_headers({"Authorization": token})
            if not posts:
                self.make_random_posts()
            else:
                self.__posts = posts
            for payload in self.__posts:
                response = self.make_requests(method="POST", url=url, data=payload, headers=self.headers)
                print(response.status_code)

    def generate_likes(self):
        print('Generate likes:')
        url = self.make_url(self.__get_posts_url)
        self.delete_from_headers("Authorization")

        posts = self.make_requests(method="GET", url=url, headers=self.headers).json()
        posts_id_list = [p_id.get('id') for p_id in posts]
        post_count = len(posts_id_list)
        for token in self.__user_tokens:
            for step in range(self.__max_likes_per_user):
                post_id = posts_id_list[random.randint(0, post_count - 1)]
                self.update_headers({"Authorization": token})
                url = self.make_url(self.__likes_url, detail=True, pk=post_id, option='like', timeout=self.__timeout)
                response = self.make_requests(method="POST", url=url, headers=self.headers)
                print(response.status_code)

    def url_validation(self):
        if not all([self.__sign_up_url,
                    self.__sign_in_url,
                    self.__posting_url,
                    self.__get_posts_url,
                    self.__likes_url]):
            raise Exception("Not all URL properties are full")

    def token_validation(self):
        if not self.__user_tokens:
            raise Exception("Generate user token")

    def settings_validation(self):
        if not all([self.__numbers_of_users,
                    self.__max_posts_per_user,
                    self.__max_likes_per_user]):
            raise Exception("Update config file.")

    def run(self):
        self.settings_validation()
        self.url_validation()
        self.generate_user_tokens()
        self.make_posts_by_user()
        self.generate_likes()

    def re_run(self):
        self.token_validation()
        self.url_validation()
        self.make_posts_by_user()
        self.generate_likes()


