

from bot_folder.bot import Bot

def main():
    bot = Bot('127.0.0.1:8000')
    bot.sign_up_url = 'api/v1/users/sign-up/'
    bot.sign_in_url = 'api/v1/users/sign-in/'
    bot.posting_url = 'api/v1/posts/post-manager/'
    bot.get_posts_url = 'api/v1/posts/post-manager/'
    bot.likes_url = 'api/v1/posts/post-manager/'
    bot.run()


if __name__ == '__main__':
    main()


