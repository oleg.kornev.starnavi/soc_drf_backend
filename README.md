Перед стартом
```bash
python -m venv venv
source venv/bin/activate
pip install --upgrade pip
pip install -r requirements.txt
```


Включить Бота
```bash
python bot_folder/run_bot
```

Настройки бота
```bash
bot_folder/config_file.py
```